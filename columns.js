import {non_negative_number, positive_number, positive_integer, isNumeric, pad_num} from "./util.js";

const COLUMNS = {
    "pos": {title: "Position", mandatory:true, type:"text", filter: x => true},
    "trans": {title: "TRANS", mandatory:true, def:0, type:"number", filter: non_negative_number},
    "sans": {title: "SANS", mandatory:true, def:0, type:"number", filter: non_negative_number},
    "period": {title: "Period", mandatory:false, type:"number", filter:positive_number},
    "title": {title: "Title", mandatory:true, def:"", type:"text", filter: x => x.length > 0},
    "thickness": {title: "Thickness", mandatory:false, def:1, type:"number", filter:positive_number},
    "j1": {title: "Temperature", mandatory:false, type:"number", filter:positive_number},
    "j2": {title: "Temperature2", mandatory:false, type:"number", filter:positive_number},
    "field": {title: "Field", mandatory:false, type:"number", filter:x => true},
    "shear1": {title: "Shear Rate 1", mandatory:false, type:"number", filter:x => true},
    "shear2": {title: "Shear Rate 2", mandatory:false, type:"number", filter:x => true},
    "shear_angle1": {title: "Shear Angle 1", mandatory:false, type:"number", filter:x => true},
    "shear_angle2": {title: "Shear Angle 2", mandatory:false, type:"number", filter:x => true},
    "pre": {title: "Pre Command", mandatory:false, type:"text", filter:x => true},
    "post": {title: "Post Command", mandatory:false, type:"text", filter:x => true},
    "rb": {title: "RB Number", mandatory:false, type:"number", filter: positive_integer}
};

export default COLUMNS;
