function non_negative_number(x) {
    return isNumeric(x) && x >= 0;
}

function non_negative_integer(x) {
    return non_negative_number(x) && Math.round(x) === Number(x);
}

function positive_number(x) {
    return isNumeric(x) && x > 0;
}

function positive_integer(x) {
    return positive_number(x) && Math.round(x) === Number(x);
}

function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function pad_num(num) {
    return String(num).padStart(2, "0");
}

function indent(lines) {
	return lines.map(line => "  " + line);
}

export {indent, non_negative_number, non_negative_integer, positive_number, positive_integer, isNumeric, pad_num};
