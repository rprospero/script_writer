const patch = IncrementalDOM.patch;
const elementOpen = IncrementalDOM.elementOpen;
const elementClose = IncrementalDOM.elementClose;
const elementVoid = IncrementalDOM.elementVoid;
const text = IncrementalDOM.text;
const ops = rxjs.operators;

import COLUMNS from "./columns.js";
import {indent, non_negative_number, non_negative_integer, positive_number, positive_integer, isNumeric, pad_num} from "./util.js";

function value_stream(target, event, test) {
    var stream =  rxjs.fromEvent(document.getElementById(target), event)
	.pipe(
	    ops.pluck("target","value"),
	    ops.startWith(document.getElementById(target).value));
    return [stream.pipe(ops.filter(test)), stream.pipe(ops.map(test))];
}

function stream_error(stream, target) {
    stream.pipe(ops.map(x => {if (x) {return "";} else{return "error";}}))
	.subscribe(x => document.getElementById(target).setAttribute("class", x));
}


function validate_model(model) {
    return model
	.every(row => Object.keys(row)
	       .filter(x => x !== "id")
	       .every(key => COLUMNS[key].filter(row[key])));
}

function create_run(id) {
    var result = {"id": id};
    for (var x in COLUMNS) {
	if("def" in COLUMNS[x]) {
	    result[x] = COLUMNS[x].def;
	}
    }
    return result;
}

function measure_line(row, base, time, aperture) {
    if (time <= 0) {return "";}
    var value = base + "('" + row.title + "', uamps=" + time;
    for (var item in row) {
	if (item === "id") {continue;}
	if (item === "trans") {continue;}
	if (item === "sans") {continue;}
	if (item === "title") {continue;}
	value += ", " + item + "=";
	if (isNumeric(row[item])) {
	    value += row[item];
	}
	else {value += "'" + row[item] + "'";}
    }
    value += ", aperture='" + aperture + "')";
    return value;
}

function new_cell(key, row) {
    var edit_event = new Event('edit_row');
    edit_event.uuid = row.id;
    edit_event.key = key;
    var local_id = row.id + "_" + key;
    var style = "";

    if (!COLUMNS[key].filter(row[key])) {
	style += " error";
    }

    elementOpen("td");
    elementVoid("input",null, null,
		"type", COLUMNS[key].type,
		"step","any",
		"value", row[key],
		"id", local_id,
		"class", style,
		"oninput", (e) => {edit_event.value = e.target.value; document.dispatchEvent(edit_event);});
    elementClose("td");
}

function render_row(row, cols) {
    var kill_event = new Event('kill_row');
    kill_event.uuid = row.id;

    elementOpen("tr", row.id);
    const columns = [...cols];
    columns.forEach(key => new_cell(key, row));
    elementOpen("td");
    elementOpen("button", null, null,
		"onclick", () => document.dispatchEvent(kill_event));
    text("➖");
    elementClose("button");
    elementClose("td");
    elementClose("tr");
}

function renderTable(rows, cols) {
    rows.forEach(row => render_row(row, cols));
}

function renderTableHead(cols) {
    elementOpen("tr");
    const columns = [...cols];
    columns.forEach(
	function(col) {
	    elementOpen("td");
	    text(COLUMNS[col].title);
	    elementClose("td");
	});
    elementClose("tr");
}

function compileTransFirst(rows, params) {
    var trans = indent(rows.map(row => measure_line(row, "do_trans", row.trans, params.trans_ap)).filter(x => x !== ""));
    var sans = indent(rows.map(row => measure_line(row, "do_sans", row.sans, params.trans_ap)).filter(x => x !== ""));

    if (trans.length > 0) {
	trans = ["if count < num_trans:", ...trans];
    }
    if (sans.length > 0) {
	sans = ["if count < num_sans:", ...sans];
    }

    return [...trans, "", ...sans];
}

function compileSansFirst(rows, params) {
    var trans = indent(rows.map(row => measure_line(row, "do_trans", row.trans, params.trans_ap)).filter(x => x !== ""));
    var sans = indent(rows.map(row => measure_line(row, "do_sans", row.sans, params.trans_ap)).filter(x => x !== ""));

    if (trans.length > 0) {
	trans = ["if count < num_trans:", ...trans];
    }
    if (sans.length > 0) {
	sans = ["if count < num_sans:", ...sans];
    }

    return [...sans, "", ...trans];
}

function compileAlternateSansFirst(rows, params) {
    var value = rows.map(function(row) {
	var sans = measure_line(row, "do_sans", row.sans, params.sans_ap);
	var trans = measure_line(row, "do_trans", row.trans, params.trans_ap);
	if (sans !== "") {
	    sans = ["if count < num_sans:", ...indent([sans])];
	} else {
	    sans = [];
	}
	if (trans !== "") {
	    trans = ["if count < num_trans:", ...indent([trans])];
	} else {
	    trans = [];
	}
	return [...sans, ...trans, ""];
    });
    return value.flat();
}

function compileAlternateTransFirst(rows, params) {
    var value = rows.map(function(row) {
	var sans = measure_line(row, "do_sans", row.sans, params.sans_ap);
	var trans = measure_line(row, "do_trans", row.trans, params.trans_ap);
	if (sans !== "") {
	    sans = ["if count < num_sans:", ...indent([sans])];
	} else {
	    sans = [];
	}
	if (trans !== "") {
	    trans = ["if count < num_trans:", ...indent([trans])];
	} else {
	    trans = [];
	}
	return [...trans, ...sans, ""];
    });
    return value.flat();
}

function compileScript(rows, params) {
    var value = ["def my_script():"];
    var header = indent([
	    "change_sample_par('Width', '" + params.width + "')",
	    "change_sample_par('height', '" + params.height + "')",
	    "change_sample_par('Geometry', '" + params.geo + "')",
	    "num_sans = " + params.sans_loops,
	    "num_trans = " + params.trans_loops,
    ]);

    var main = [];
    switch (params.mode) {
    case "sans":
	main = indent( compileSansFirst(rows, params)); break;
    case "trans":
	main = indent( compileTransFirst(rows, params)); break;
    case "alt_sans":
	main = indent( compileAlternateSansFirst(rows, params)); break;
    case "alt_trans":
	main = indent( compileAlternateTransFirst(rows, params)); break;
    }

    if (main.filter(x => x.trim() !== "").length > 0) {
	main = indent([
	    "for count in range(max(num_sans, num_trans)):",
	    ...indent(main)]);
    } else {
	main = [];
    }
    value = [...value, ...header, "", ...main];
    return value.join("\r\n");
}

function save_script(script) {
    var file = new Blob([script], {type: "text/plain"});
    elementOpen("a", null, null,
		"href", URL.createObjectURL(file),
		 "download","myscript.py");
    elementOpen("Button");
    text("Save script");
    elementClose("Button");
    elementClose("a");
}

function renderTime(beam_current, move_time, rows, now) {
    var value = 0;
    rows.forEach(row => value += Number(row.sans)/beam_current + Number(row.trans)/beam_current + Number(move_time)/60);
    value = value.toFixed(2);

    var final_time = new Date(now);

    final_time.setTime(final_time.getTime() + value*3600*1000);

    var result = "Measurement will finish in " + value + " hours at approximately ";

    result += pad_num(final_time.getHours()) + ":" + pad_num(final_time.getMinutes());
    if (final_time.getDate() !== now.getDate()) {
	result += " on " + final_time.getFullYear() + "–" +
	    pad_num(final_time.getMonth() + 1) +
	    "–" + pad_num(final_time.getDate());
    }

    text(result);
}

function update_model(model, event) {
    switch(event.type) {
    case "new":
	return [...model, create_run(event.id)];
    case "kill":
	return model.filter((x, idx, arr) => x.id !== event.id);
    case "edit":
	const idx = model.findIndex(x => x.id === event.id);
	var value = model.slice(0);
	if (idx >= 0) {value[idx][event.key] = event.value;}
	return value;
    }
}


var active_cols;

function setup() {
    patch(document.getElementById("active_cols"), () => {
	for (var x in COLUMNS) {
	    elementOpen("label", "label_"+x, null, "for", "checkbox_"+x);
	    text(COLUMNS[x].title);
	    elementClose("label");
	    if(COLUMNS[x].mandatory) {
		elementVoid("input", x, null,
			    "checked", true,
			    "disabled", true,
			    "type", "checkbox",
			    "id", "checkbox_"+x);
	    }
	    else {
		elementVoid("input", x, null,
			    "type", "checkbox",
			    "id", "checkbox_"+x);
	    }
	}
    });
    var obs = [];
    for (var x in COLUMNS) {
	let y = x;
	obs.push(
	    rxjs.fromEvent(document.getElementById("checkbox_"+x), "change")
		.pipe(
		    ops.pluck("target","checked"),
		    ops.startWith(document.getElementById("checkbox_"+x).checked),
		    ops.map(value => [y, value])));
    }
    active_cols = rxjs.combineLatest(obs)
	.pipe(ops.map(items => {
	    return items.filter(item => item[1]).map(item => item[0]);
	}));
}

setup();

var counter = rxjs.range();

var new_rows = rxjs.fromEvent(document.getElementById("new_row"), "click")
    .pipe(ops.map((x) => ({"type": "new", "id": x.timeStamp})));

// new_rows.pipe(ops.scan((x, _) => x+1,0)).subscribe(console.log);

var kill_rows = rxjs.fromEvent(document, "kill_row")
    .pipe(ops.map(x => ({"type":"kill", "id":x.uuid})));

var edit_rows = rxjs.fromEvent(document, "edit_row")
    .pipe(ops.map(x => ({"type":"edit", "id":x.uuid, "key": x.key, "value":x.value})));

active_cols.subscribe(x => patch(document.getElementById("table_head"), () => renderTableHead(x)));

var model = rxjs.merge(new_rows, kill_rows, edit_rows)
    .pipe(
	ops.debounceTime(50),
	ops.scan(update_model, []));

var model_valid = model.pipe(ops.map(validate_model));

rxjs.combineLatest(model, active_cols)
    .subscribe(x => patch(document.getElementById("options"),
			 () => renderTable(x[0], x[1])));

var [mode, mode_valid]  = value_stream("mode", "input", x => true);
var [sample_geometry, sample_geometry_valid]  = value_stream("sample_geometry", "input", x => true);
var [sample_width, sample_width_valid]  = value_stream("sample_width", "input", positive_number);
var [sample_height, sample_height_valid]  = value_stream("sample_height", "input", positive_number);
var [sans_loops, sans_loops_valid]  = value_stream("sans_loops", "input", non_negative_integer);
var [trans_loops, trans_loops_valid]  = value_stream("trans_loops", "input", non_negative_integer);
var [sans_ap, sans_ap_valid]  = value_stream("sans_ap", "input", x => true);
var [trans_ap, trans_ap_valid]  = value_stream("trans_ap", "input", x => true);
var params = rxjs.combineLatest([
    sample_geometry, sample_width, sample_height, mode, sans_loops, trans_loops,
    sans_ap, trans_ap])
    .pipe(ops.map(x => ({geo: x[0], width:x[1], height: x[2], mode: x[3],
			sans_loops: x[4], trans_loops: x[5], sans_ap: x[6],
			trans_ap: x[7]})));

stream_error(sans_loops_valid, "sans_loops");
stream_error(trans_loops_valid, "trans_loops");
stream_error(sample_width_valid, "sample_width");
stream_error(sample_height_valid, "sample_height");

var valid_state = rxjs.combineLatest([
    model_valid,
    sample_geometry_valid, sample_width_valid, sample_height_valid, mode_valid, sans_loops_valid, trans_loops_valid,
    sans_ap_valid, trans_ap_valid])
    .pipe(ops.map(x => x.every(x => x)));

var script = rxjs.combineLatest(model, params)
    .pipe(ops.map(x => compileScript(x[0], x[1])));

valid_state.subscribe(x => {
    if(x) {document.getElementById("save_script").removeAttribute("hidden");}
    else {document.getElementById("save_script").setAttribute("hidden", true);}
});

script.subscribe(x => patch(document.getElementById("script"), () => text(x)));
script.subscribe(x => patch(document.getElementById("save_script"),
			   () => save_script(x)));

var minutely = rxjs.interval(15000)
    .pipe(ops.map(x => new Date()), ops.startWith(new Date()));


var [beam_current, beam_current_valid]  = value_stream("uamps", "input", positive_number);

var [movement, movement_valid]  = value_stream("movement", "input", positive_number);

stream_error(beam_current_valid, "uamps");
stream_error(movement_valid, "movement");

rxjs.combineLatest(beam_current, movement, model, minutely)
    .subscribe(x => patch(document.getElementById("time"), () => renderTime(x[0], x[1], x[2], x[3])));
